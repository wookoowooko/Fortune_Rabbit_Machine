package com.fortune.machine.rabbit.pg

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RabbitViewModel @Inject constructor(
    private val rabbitGenerator: RabbitGenerator,
) : ViewModel() {

    private val _rabbitBalance = MutableStateFlow(10000)
    val rabbitBalance = _rabbitBalance.asStateFlow()


    private val _rabbitTotal = MutableStateFlow(50)
    val rabbitTotal = _rabbitTotal.asStateFlow()


    fun setRabbitWinBalance() {
        _rabbitBalance.value = (_rabbitBalance.value + _rabbitTotal.value * 3f).toInt()
    }

    fun setRabbitReminderBank() {
        _rabbitBalance.value = _rabbitBalance.value - _rabbitTotal.value
    }

    fun setRabbitDefaultBet() {
        if (_rabbitBalance.value > 50) _rabbitTotal.value = 50
        else _rabbitTotal.value = 0

    }

    fun incOrDecRabbitTotal(func: String) {
        when (func) {
            "inc" -> {
                if (_rabbitBalance.value > _rabbitTotal.value) _rabbitTotal.value =
                    _rabbitTotal.value + 50
            }

            "dec" -> {
                if (_rabbitTotal.value > 0) _rabbitTotal.value = _rabbitTotal.value - 50
            }
        }
    }


    private val _rabbitListOfImages1: MutableStateFlow<List<Int>> =
        MutableStateFlow(listOf(R.drawable.ic_1, R.drawable.ic_2, R.drawable.ic_3, R.drawable.ic_4))
    val rabbitListOfImages1 = _rabbitListOfImages1.asStateFlow()

    private val _rabbitListOfImages2: MutableStateFlow<List<Int>> =
        MutableStateFlow(listOf(R.drawable.ic_5, R.drawable.ic_6, R.drawable.ic_7, R.drawable.ic_1))
    val rabbitListOfImages2 = _rabbitListOfImages2.asStateFlow()

    private val _rabbitListOfImages3: MutableStateFlow<List<Int>> =
        MutableStateFlow(listOf(R.drawable.ic_2, R.drawable.ic_3, R.drawable.ic_4, R.drawable.ic_5))
    val rabbitListOfImages3 = _rabbitListOfImages3.asStateFlow()

    private val _rabbitListOfImages4: MutableStateFlow<List<Int>> =
        MutableStateFlow(listOf(R.drawable.ic_6, R.drawable.ic_7, R.drawable.ic_1, R.drawable.ic_2))
    val rabbitListOfImages4 = _rabbitListOfImages4.asStateFlow()


    private val _rabbitListWordsFirst = MutableStateFlow<List<String>>(emptyList())
    var rabbitListWordsFirst = _rabbitListWordsFirst.asStateFlow()

    private val _rabbitListWordsSecond = MutableStateFlow<List<String>>(emptyList())
    var rabbitListWordsSecond = _rabbitListWordsSecond.asStateFlow()

    private val _rabbitListWordsThird = MutableStateFlow<List<String>>(emptyList())
    var rabbitListWordsThird = _rabbitListWordsThird.asStateFlow()

    private val _rabbitListWordsFourth = MutableStateFlow<List<String>>(emptyList())
    var rabbitListWordsFourth = _rabbitListWordsFourth.asStateFlow()


    fun rabbitGenerateRandomListsFinalFirst() {
        viewModelScope.launch {
            val rabbitListOfImages1 = rabbitGenerator.rabbitGenerateRandomListsFinal(4)
            val rabbitWordsList1 =
                rabbitListOfImages1.map { rabbitGenerator.rabbitiToWordMap[it] ?: "неизвестно" }
            _rabbitListOfImages1.value = rabbitListOfImages1
            _rabbitListWordsFirst.value = rabbitWordsList1
        }
    }

    fun rabbitGenerateRandomListsFinalSecond() {
        viewModelScope.launch {
            val rabbitListOfImages2 = rabbitGenerator.rabbitGenerateRandomListsFinal(4)
            val rabbitWordsList2 =
                rabbitListOfImages2.map { rabbitGenerator.rabbitiToWordMap[it] ?: "неизвестно" }
            _rabbitListOfImages2.value = rabbitListOfImages2
            _rabbitListWordsSecond.value = rabbitWordsList2
        }
    }

    fun rabbitGenerateRandomListsFinalThird() {
        viewModelScope.launch {
            val rabbitListOfImages3 = rabbitGenerator.rabbitGenerateRandomListsFinal(4)
            val rabbitWordsList3 =
                rabbitListOfImages3.map { rabbitGenerator.rabbitiToWordMap[it] ?: "неизвестно" }
            _rabbitListOfImages3.value = rabbitListOfImages3
            _rabbitListWordsThird.value = rabbitWordsList3
        }
    }

    fun rabbitGenerateRandomListsFinalFourth() {
        viewModelScope.launch {
            val rabbitListOfImages4 = rabbitGenerator.rabbitGenerateRandomListsFinal(4)
            val rabbitWordsList4 =
                rabbitListOfImages4.map { rabbitGenerator.rabbitiToWordMap[it] ?: "неизвестно" }
            _rabbitListOfImages4.value = rabbitListOfImages4
            _rabbitListWordsFourth.value = rabbitWordsList4
        }
    }


}

