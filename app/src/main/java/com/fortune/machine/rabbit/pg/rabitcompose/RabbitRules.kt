package com.fortune.machine.rabbit.pg.rabitcompose

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.navigation.NavHostController
import com.fortune.machine.rabbit.pg.R
import com.fortune.machine.rabbit.pg.RabbitRulesDTO.rabbitRulesList


@Composable
fun RabbitRules(rabbitNav: NavHostController) {
    Image(
        painter = painterResource(id = R.drawable.loadingsbeck),
        contentDescription = null,
        modifier = Modifier.fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {

        Image(
            painter = painterResource(id = R.drawable.clos),
            contentDescription = null,
            modifier = Modifier
                .size(60.dp)
                .align(Alignment.TopEnd)
                .padding(
                    top = 20.dp,
                    end = 20.dp
                )
                .clickable {
                    rabbitNav.navigate(RabbitNavRout.rabbitChoose)
                },
            contentScale = ContentScale.Crop
        )

        Column(
            Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {


            Box(
                modifier = Modifier
                    .weight(20f)
                    .zIndex(2f),
                contentAlignment = Alignment.BottomCenter
            ) {


                Image(
                    painter = painterResource(id = R.drawable.rules),
                    contentDescription = null,
                    modifier = Modifier.fillMaxWidth(),
                    contentScale = ContentScale.Crop
                )
            }

            Box(
                modifier = Modifier
                    .weight(70f)
                    .paint(
                        painterResource(id = R.drawable.bigbillboard),
                        contentScale = ContentScale.Crop
                    ),
                contentAlignment = Alignment.Center
            ) {

                LazyColumn(
                    modifier = Modifier
                        .fillMaxWidth(0.5f).padding(vertical = 20.dp)
                ) {
                    itemsIndexed(rabbitRulesList) { _, rabbitItem ->
                        Text(
                            text = rabbitItem.rabbitRule,
                            modifier = Modifier.padding(vertical = 10.dp),
                            fontSize = 28.sp,
                            fontFamily = FontFamily(Font(R.font.nerk)),
                            color = RulesTrackColor
                        )
                    }
                }
            }

            Box(
                modifier = Modifier
                    .weight(10f)
                    .background(Color.Red),
                contentAlignment = Alignment.TopCenter
            ) {

            }
        }

    }
}

