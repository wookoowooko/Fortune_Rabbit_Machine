package com.fortune.machine.rabbit.pg

import android.app.Application
import com.backendless.Backendless
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class RabbitApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Backendless.initApp(
            this,
            "4E031563-8188-EB1F-FFA2-88D5B20E7E00",
            "EC0971DE-9A10-4103-AB26-8C3AC4A21282"
        )
    }
}