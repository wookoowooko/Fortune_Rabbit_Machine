package com.fortune.machine.rabbit.pg

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RabbitDI {

    @Provides
    @Singleton
    fun provideRabbitGenerator(): RabbitGenerator {
        return RabbitGenerator()
    }

}