package com.fortune.machine.rabbit.pg

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.hilt.navigation.compose.hiltViewModel
import com.fortune.machine.rabbit.pg.rabbitUtils.RabbitLessViewModel
import com.fortune.machine.rabbit.pg.rabitcompose.RabbitNav
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class RabbitMainActivity : ComponentActivity() {


    private val rabbitContextActiv = this
    private val rabbitLessViewModel by viewModels<RabbitLessViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val rabbitViewModel = hiltViewModel<RabbitViewModel>()
            RabbitNav(rabbitContextActiv, rabbitViewModel, rabbitLessViewModel)
        }
    }
}
