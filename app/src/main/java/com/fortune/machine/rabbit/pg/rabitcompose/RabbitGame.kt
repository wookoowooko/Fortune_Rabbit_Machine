package com.fortune.machine.rabbit.pg.rabitcompose

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import com.fortune.machine.rabbit.pg.R
import com.fortune.machine.rabbit.pg.RabbitViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@Composable
fun RabbitGame(rabbitViewModel: RabbitViewModel) {

    val rabitBalance by rabbitViewModel.rabbitBalance.collectAsState()
    val rabitTotal by rabbitViewModel.rabbitTotal.collectAsState()


    val rabbitListOfImages1 by rabbitViewModel.rabbitListOfImages1.collectAsState()
    val rabbitListOfImages2 by rabbitViewModel.rabbitListOfImages2.collectAsState()
    val rabbitListOfImages3 by rabbitViewModel.rabbitListOfImages3.collectAsState()
    val rabbitListOfImages4 by rabbitViewModel.rabbitListOfImages4.collectAsState()

    val rabbitText1 by rabbitViewModel.rabbitListWordsFirst.collectAsState()
    val rabbitText2 by rabbitViewModel.rabbitListWordsSecond.collectAsState()
    val rabbitText3 by rabbitViewModel.rabbitListWordsThird.collectAsState()
    val rabbitText4 by rabbitViewModel.rabbitListWordsFourth.collectAsState()

    val rabbitCoroutine = rememberCoroutineScope()

    var isRabbitSpinButtonPressed by remember {
        mutableStateOf(false)
    }
    var rabbitSlotBoxHeight by remember { mutableIntStateOf(0) }


    val rabbitTranslationYAnimation = rememberInfiniteTransition(label = "")

    var isRabbitAnimationFirstRunning by remember { mutableStateOf(false) }
    var isRabbitAnimationSecondRunning by remember { mutableStateOf(false) }
    var isRabbitAnimationThirdRunning by remember { mutableStateOf(false) }
    var isRabbitAnimationFourthRunning by remember { mutableStateOf(false) }


    var showRabbitFirstCompareRow by remember { mutableStateOf(false) }
    var showRabbitSecCompareRow by remember { mutableStateOf(false) }
    var showRabbitThirdCompareRow by remember { mutableStateOf(false) }
    var showRabbitFourthCompareRow by remember { mutableStateOf(false) }



    LaunchedEffect(isRabbitAnimationFirstRunning) {
        while (isRabbitAnimationFirstRunning) {
            rabbitViewModel.rabbitGenerateRandomListsFinalFirst()
            delay(10)
        }
    }
    LaunchedEffect(isRabbitAnimationSecondRunning) {
        while (isRabbitAnimationSecondRunning) {
            rabbitViewModel.rabbitGenerateRandomListsFinalSecond()
            delay(10)
        }
    }
    LaunchedEffect(isRabbitAnimationThirdRunning) {
        while (isRabbitAnimationThirdRunning) {
            rabbitViewModel.rabbitGenerateRandomListsFinalThird()
            delay(20)
        }
    }
    LaunchedEffect(isRabbitAnimationFourthRunning) {
        while (isRabbitAnimationFourthRunning) {
            rabbitViewModel.rabbitGenerateRandomListsFinalFourth()
            delay(30)
        }
    }


    val rabbitTranslationFirstColumn by rabbitTranslationYAnimation.animateFloat(
        initialValue = -(rabbitSlotBoxHeight / 40).toFloat(),
        targetValue = (rabbitSlotBoxHeight / 40).toFloat(),
        animationSpec = infiniteRepeatable(
            animation = tween(80, easing = LinearEasing), repeatMode = RepeatMode.Restart
        ),
        label = ""
    )
    val rabbitTranslationSecondColumn by rabbitTranslationYAnimation.animateFloat(
        initialValue = -(rabbitSlotBoxHeight / 40).toFloat(),
        targetValue = (rabbitSlotBoxHeight / 40).toFloat(),
        animationSpec = infiniteRepeatable(
            animation = tween(90, easing = LinearEasing), repeatMode = RepeatMode.Restart
        ),
        label = ""
    )
    val rabbitTranslationThirdColumn by rabbitTranslationYAnimation.animateFloat(
        initialValue = -(rabbitSlotBoxHeight / 40).toFloat(),
        targetValue = (rabbitSlotBoxHeight / 40).toFloat(),
        animationSpec = infiniteRepeatable(
            animation = tween(70, easing = LinearEasing), repeatMode = RepeatMode.Restart
        ),
        label = ""
    )
    val rabbitTranslationFourthColumn by rabbitTranslationYAnimation.animateFloat(
        initialValue = -(rabbitSlotBoxHeight / 40).toFloat(),
        targetValue = (rabbitSlotBoxHeight / 40).toFloat(),
        animationSpec = infiniteRepeatable(
            animation = tween(60, easing = LinearEasing), repeatMode = RepeatMode.Restart
        ),
        label = ""
    )

    Image(
        painter = painterResource(id = R.drawable.loadingsbeck),
        contentDescription = null,
        modifier = Modifier.fillMaxSize(),
        contentScale = ContentScale.Crop
    )



    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            modifier = Modifier
                .weight(0.3f)
                .zIndex(44f)
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {


            Column(
                Modifier
                    .fillMaxWidth()
                    .zIndex(123f)
                    .align(Alignment.BottomCenter),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(0.6f),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceEvenly
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.game_balance),
                        contentDescription = null,
                        modifier = Modifier.scale(3f)
                    )

                    Image(
                        painter = painterResource(id = R.drawable.game_total),
                        contentDescription = null,
                        modifier = Modifier.scale(3f)
                    )
                }


                Row(
                    modifier = Modifier
                        .fillMaxWidth(0.5f)
                        .padding(bottom = 10.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    Text(
                        text = rabitBalance.toString(),
                        fontFamily = FontFamily(Font(R.font.nerk)),
                        color = RulesTrackColor,
                        fontSize = 32.sp
                    )

                    Row(
                        modifier = Modifier,
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "-",
                            modifier = Modifier
                                .padding(end = 4.dp)
                                .clickable {
                                    rabbitViewModel.incOrDecRabbitTotal("dec")
                                },
                            fontFamily = FontFamily(Font(R.font.nerk)),
                            color = RulesTrackColor,
                            fontSize = 32.sp
                        )
                        Text(
                            text = rabitTotal.toString(),
                            fontFamily = FontFamily(Font(R.font.nerk)),
                            color = RulesTrackColor,
                            fontSize = 32.sp
                        )
                        Text(
                            text = "+",
                            modifier = Modifier
                                .padding(start = 4.dp)
                                .clickable {
                                    rabbitViewModel.incOrDecRabbitTotal("inc")
                                },
                            fontFamily = FontFamily(Font(R.font.nerk)),
                            color = RulesTrackColor,
                            fontSize = 32.sp
                        )
                    }

                }
            }

            Image(
                painter = painterResource(id = R.drawable.game_lenta),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth(0.8f)
                    .align(Alignment.BottomCenter)
                    .offset(y = 30.dp)
                    .zIndex(3f),
                contentScale = ContentScale.Crop
            )

            Image(
                painter = painterResource(id = R.drawable.gamezaec),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.Crop
            )


        }
        Box(
            modifier = Modifier
                .weight(0.7f)
                .zIndex(4f)
                .fillMaxWidth(),
            contentAlignment = Alignment.Center


        ) {


            Column(
                Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(70f),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.game_villboard),
                        contentDescription = null,
                        modifier = Modifier.scale(3.3f)
                    )

                    Box(modifier = Modifier
                        .fillMaxSize(0.76f)
                        .onGloballyPositioned {
                            rabbitSlotBoxHeight = it.size.height
                        }

                    ) {

                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Column(
                                modifier = if (isRabbitAnimationFirstRunning) Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .offset(y = rabbitTranslationFirstColumn.dp)
                                else Modifier
                                    .fillMaxSize()
                                    .weight(1f),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                rabbitListOfImages1.forEach {
                                    Image(
                                        painter = painterResource(id = it),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .weight(1f)
                                            .fillMaxSize()
                                            .padding(10.dp)
                                    )
                                }
                            }
                            Column(
                                modifier = if (isRabbitAnimationSecondRunning) Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .offset(y = rabbitTranslationSecondColumn.dp)
                                else Modifier
                                    .fillMaxSize()
                                    .weight(1f),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                rabbitListOfImages2.forEach {
                                    Image(
                                        painter = painterResource(id = it),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .weight(1f)
                                            .fillMaxSize()
                                            .padding(10.dp)
                                    )
                                }
                            }
                            Column(
                                modifier = if (isRabbitAnimationThirdRunning) Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .offset(y = rabbitTranslationThirdColumn.dp)
                                else Modifier
                                    .fillMaxSize()
                                    .weight(1f),

                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                rabbitListOfImages3.forEach {
                                    Image(
                                        painter = painterResource(id = it),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .weight(1f)
                                            .fillMaxSize()
                                            .padding(10.dp)
                                    )
                                }
                            }
                            Column(
                                modifier = if (isRabbitAnimationFourthRunning) Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .offset(y = rabbitTranslationFourthColumn.dp)
                                else Modifier
                                    .fillMaxSize()
                                    .weight(1f),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                rabbitListOfImages4.forEach {
                                    Image(
                                        painter = painterResource(id = it),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .weight(1f)
                                            .fillMaxSize()
                                            .padding(10.dp)
                                    )
                                }
                            }
                        }

                        Column(
                            Modifier.fillMaxWidth(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {


                            OutlinedCard(
                                border = if (showRabbitFirstCompareRow) BorderStroke(
                                    2.dp, WinColor
                                ) else BorderStroke(0.dp, Color.Transparent),
                                modifier = Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .padding(10.dp),
                                colors = CardDefaults.cardColors(
                                    containerColor = Color.Transparent
                                )
                            ) {

                            }
                            OutlinedCard(
                                border = if (showRabbitSecCompareRow) BorderStroke(
                                    2.dp, WinColor
                                ) else BorderStroke(0.dp, Color.Transparent),
                                modifier = Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .padding(10.dp),
                                colors = CardDefaults.cardColors(
                                    containerColor = Color.Transparent
                                )
                            ) {

                            }
                            OutlinedCard(
                                border = if (showRabbitThirdCompareRow) BorderStroke(
                                    2.dp, WinColor
                                ) else BorderStroke(0.dp, Color.Transparent),
                                modifier = Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .padding(10.dp),
                                colors = CardDefaults.cardColors(
                                    containerColor = Color.Transparent
                                )
                            ) {

                            }
                            OutlinedCard(
                                border = if (showRabbitFourthCompareRow) BorderStroke(
                                    2.dp, WinColor
                                ) else BorderStroke(0.dp, Color.Transparent),
                                modifier = Modifier
                                    .fillMaxSize()
                                    .weight(1f)
                                    .padding(10.dp),
                                colors = CardDefaults.cardColors(
                                    containerColor = Color.Transparent
                                )
                            ) {

                            }
                        }
                    }
                }
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(30f),
                    contentAlignment = Alignment.Center
                ) {

                    Image(painter = painterResource(id = R.drawable.game_btn),
                        contentDescription = null,
                        modifier = if (rabitBalance != 0 && !isRabbitSpinButtonPressed) Modifier
                            .scale(
                                2.7f
                            )
                            .clickable {

                                rabbitViewModel.setRabbitReminderBank()

                                isRabbitSpinButtonPressed = true

                                showRabbitFirstCompareRow = false
                                showRabbitSecCompareRow = false
                                showRabbitThirdCompareRow = false
                                showRabbitFourthCompareRow = false

                                rabbitCoroutine.launch {
                                    delay(120)
                                    isRabbitAnimationFirstRunning = true
                                    isRabbitAnimationSecondRunning = true
                                    isRabbitAnimationThirdRunning = true
                                    isRabbitAnimationFourthRunning = true


                                    delay(400)
                                    isRabbitAnimationFirstRunning = false
                                    delay(400)
                                    isRabbitAnimationSecondRunning = false
                                    delay(400)
                                    isRabbitAnimationThirdRunning = false
                                    delay(400)
                                    isRabbitAnimationFourthRunning = false
                                    delay(200)
                                    isRabbitSpinButtonPressed = false





                                    if (
                                        (rabbitText1[0] == rabbitText2[0] && rabbitText1[0] == rabbitText3[0]) ||
                                        (rabbitText1[0] == rabbitText2[0] && rabbitText1[0] == rabbitText3[0] && rabbitText1[0] == rabbitText4[0]) ||
                                        (rabbitText2[0] == rabbitText3[0] && rabbitText2[0] == rabbitText4[0]) ||
                                        (rabbitText1[0] == rabbitText2[0] && rabbitText1[0] == rabbitText4[0])
                                    ) {
                                        showRabbitFirstCompareRow = true
                                        rabbitViewModel.setRabbitWinBalance()

                                    } else if ((rabbitText1[1] == rabbitText2[1] && rabbitText1[1] == rabbitText3[1]) ||
                                        (rabbitText1[1] == rabbitText2[1] && rabbitText1[1] == rabbitText3[1] && rabbitText1[1] == rabbitText4[1]) ||
                                        (rabbitText2[1] == rabbitText3[1] && rabbitText2[1] == rabbitText4[1]) ||
                                        (rabbitText1[1] == rabbitText2[1] && rabbitText1[1] == rabbitText4[1])
                                    ) {
                                        showRabbitSecCompareRow = true
                                        rabbitViewModel.setRabbitWinBalance()

                                    } else if ((rabbitText1[2] == rabbitText2[2] && rabbitText1[2] == rabbitText3[2]) ||
                                        (rabbitText1[2] == rabbitText2[2] && rabbitText1[2] == rabbitText3[2] && rabbitText1[2] == rabbitText4[2]) ||
                                        (rabbitText2[2] == rabbitText3[2] && rabbitText2[2] == rabbitText4[2]) ||
                                        (rabbitText1[2] == rabbitText2[2] && rabbitText1[2] == rabbitText4[2])
                                    ) {
                                        showRabbitThirdCompareRow = true
                                        rabbitViewModel.setRabbitWinBalance()

                                    } else if ((rabbitText1[3] == rabbitText2[3] && rabbitText1[3] == rabbitText3[3]) ||
                                        (rabbitText1[3] == rabbitText2[3] && rabbitText1[3] == rabbitText3[3] && rabbitText1[3] == rabbitText4[3]) ||
                                        (rabbitText2[3] == rabbitText3[3] && rabbitText2[3] == rabbitText4[3]) ||
                                        (rabbitText1[3] == rabbitText2[3] && rabbitText1[3] == rabbitText4[3])
                                    ) {
                                        showRabbitFourthCompareRow = true
                                        rabbitViewModel.setRabbitWinBalance()
                                    }


                                    rabbitViewModel.setRabbitDefaultBet()

                                }
                            }
                        else Modifier.scale(2.7f))
                    Image(
                        painter = painterResource(id = R.drawable.spin),
                        contentDescription = null,
                        modifier = Modifier.scale(2.7f)
                    )
                }
            }

        }
    }

}