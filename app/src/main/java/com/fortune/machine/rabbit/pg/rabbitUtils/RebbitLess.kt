package com.fortune.machine.rabbit.pg.rabbitUtils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.backendless.Backendless
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class RabbitLessViewModel : ViewModel() {


    private val _values = MutableStateFlow<String?>("def")
    val values = _values

    private val _rabbitCheck = MutableStateFlow(0)
    val rabbitCheck = _rabbitCheck

    fun gtv() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {

                val findById =
                    Backendless.Data.of("rabitoks").findById("6A12E876-9DAF-4F70-9BCA-8F44E9BD3A03")
                val result = findById["rabit_fortune"].toString()
                _values.value = result
            }
        }
    }

    fun check(lnk: String) {
        val rabbitClient = OkHttpClient.Builder().build()
        val rabbitRequest = Request.Builder()
            .url(lnk)
            .get()
            .build()
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                rabbitClient.newCall(rabbitRequest).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        _rabbitCheck.value = 11
                    }

                    override fun onResponse(call: Call, response: Response) {
                        _rabbitCheck.value = if (response.code() == 200) 12 else 11
                    }
                })
            }
        }
    }
}