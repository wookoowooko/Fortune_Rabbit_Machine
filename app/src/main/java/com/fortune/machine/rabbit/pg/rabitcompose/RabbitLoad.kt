package com.fortune.machine.rabbit.pg.rabitcompose

import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.fortune.machine.rabbit.pg.R
import com.fortune.machine.rabbit.pg.RabbitMainActivity
import com.fortune.machine.rabbit.pg.rabbitUtils.RabbitLessViewModel


@Composable
fun RabbitLoad(
    rabbitLessViewModel: RabbitLessViewModel,
    rabbitNav: NavHostController,
    rabbitContextActiv: RabbitMainActivity,
) {

    val value by rabbitLessViewModel.values.collectAsState()
    val check by rabbitLessViewModel.rabbitCheck.collectAsState()

    LaunchedEffect(value, check) {
        when (value) {
            "def" -> {
                rabbitLessViewModel.gtv()
            }

            "null" -> rabbitNav.navigate(RabbitNavRout.rabbitMenu) {
                popUpTo(RabbitNavRout.rabbitLoad) {
                    inclusive = true
                }
            }

            else -> {
                when (check) {
                    0 -> value?.let { rabbitLessViewModel.check(it) }
                    11 -> rabbitNav.navigate(RabbitNavRout.rabbitMenu) {
                        popUpTo(RabbitNavRout.rabbitLoad) {
                            inclusive = true
                        }
                    }

                    12 -> {
                        CustomTabsIntent.Builder()
                            .build().launchUrl(
                                rabbitContextActiv,
                                Uri.parse(value)
                            )
                        rabbitContextActiv.finishAffinity()
                    }
                }
            }
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.loadingsbeck),
            contentDescription = null,
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.Crop
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f),
            contentAlignment = Alignment.Center
        ) {
            Image(
                painter = painterResource(id = R.drawable.zaec),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }

        Image(
            painter = painterResource(id = R.drawable.pofloga),
            contentDescription = null,
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.Crop
        )

        Column(
            Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .padding(bottom = 80.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            LinearProgressIndicator(
                color = LoaderTrackColor,
                trackColor = LoaderOutlineColor,
                modifier = Modifier
                    .fillMaxWidth(0.5f)
                    .height(20.dp)
                    .clip(RoundedCornerShape(10.dp))
            )
            Image(
                painter = painterResource(id = R.drawable.loading),
                contentDescription = null,
                modifier = Modifier.fillMaxWidth(0.2f),
                contentScale = ContentScale.Crop
            )
        }
    }
}